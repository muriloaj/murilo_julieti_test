class Lines:

    def set_line1(self, x1, x2):
        temp = [x1, x2]
        list.sort(temp)
        self.l1_1 = temp[0]
        self.l1_2 = temp[1]

    def set_line2(self, x1, x2):
        temp = [x1, x2]
        list.sort(temp)
        self.l2_1 = temp[0]
        self.l2_2 = temp[1]

    def print(self):
        is_it_overlapped = "Overlapped" if self.is_overlapped() else "Isolated"
        print("Line a({0},{1}) b({2},{3}) : {4}".format(self.l1_1, self.l1_2, self.l2_1, self.l2_2, is_it_overlapped))

    # To check if is overlapped one of the position must to be inside the other interval
    # assumptions:
    # - connections like 1,2 with 2,3 is considered an overlap
    # - if one line is not defined, it should throw an exception.
    # Formula:
    #    a1 <= b1 <=a2 OR  a1 <= b2 <=a2
    def is_overlapped(self):
        try:
            a1 = self.l1_1
            a2 = self.l1_2
            b1 = self.l2_1
            b2 = self.l2_2
            return (a1 <= b1 and b1 <= a2) or (a1 <= b2 and b2 <= a2)
        except AttributeError:
            raise AttributeError
        except TypeError:
            raise TypeError
