#Goal:
Your goal for this question is to write a program that accepts two lines (x1,x2) and (x3,x4) on the
x-axis and returns whether they overlap. As an example, (1,5) and (2,6) overlaps but not (1,5)
and (6,8).

To check if is overlapped one of the position must to be inside the other interval
## Assumptions:
 - connections like 1,2 with 2,3 is considered an overlap;
 - if one line is not defined, it should throw an exception;
 - Formula: *a1 <= b1 <=a2* OR  *a1 <= b2 <=a2*, considering line 1 (a1,a2) and line 2 (b1,b2).
  

#Structure:

## Prerequisites
There is no necessity to install additional Python dependencies.

###Python version
Python 3.7.2


#To Run
The _Lines.py_ is just composed by a class, to work as library. 
The _test_overlapped_lines.py_ is a test suite but, to simplify the execution, the it can be executed just with the command ```$ python _test_overlapped_lines.py_```.


#Testcases:

The test cases cover the scenarios:

- Not connected lines should return not overlapped status;
- Overlapped lines, should return overlapped status;
- Just connected lines, e.g. (1,2)(2,3), should return overlapped status;
- Empty lines and non numeric values should raise an exception.

# Missing functionalities:
Improve the tests engine to run in a bigger project.
It was developed to support only 2 samples, it should be improved to receive many in a list, e.g.. 
It runs over terminal but it is not adapted for any test engine even it have extended _unittest.TestCase_.

