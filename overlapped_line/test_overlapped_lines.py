import unittest

from Lines import Lines


class Overlapped_lines_test(unittest.TestCase):
    def test_overlapped_lines_01(self):
        test_case = Lines()
        test_case.set_line1(1, 3)
        test_case.set_line2(2, 4)
        self.assertTrue(test_case.is_overlapped())

    def test_overlapped_lines_02(self):
        test_case = Lines()
        test_case.set_line1(2, 4)
        test_case.set_line2(1, 3)
        self.assertTrue(test_case.is_overlapped())

    def test_isolated_lines_03(self):
        test_case = Lines()
        test_case.set_line1(1, 2)
        test_case.set_line2(3, 4)
        self.assertFalse(test_case.is_overlapped())

    def test_isolated_lines_04(self):
        test_case = Lines()
        test_case.set_line1(3, 4)
        test_case.set_line2(1, 2)
        self.assertFalse(test_case.is_overlapped())

    def test_missing_line_01(self):
        try:
            test_case = Lines()
            test_case.set_line1(1, 2)
            self.assertTrue(test_case.is_overlapped())
        except AttributeError:
            self.assertTrue(True)

    def test_missing_line_02(self):
        try:
            test_case = Lines()
            test_case.set_line2(1, 2)
            test_case.is_overlapped()
            self.assertTrue(False)
        except AttributeError:
            self.assertTrue(True)

    def test_missing_line_03(self):
        try:
            test_case = Lines()
            test_case.is_overlapped()
            self.assertTrue(False)
        except AttributeError:
            self.assertTrue(True)

    def test_non_numeric_field(self):
        try:
            test_case = Lines()
            test_case.set_line1('test', 2)
            test_case.set_line2(1, 2)
            test_case.is_overlapped()
            self.assertTrue(False)
        except TypeError:
            self.assertTrue(True)


### Become code Runnable
unittest.main()
