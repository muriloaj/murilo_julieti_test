import unittest

from Strings import Strings


class Strings_compare_test(unittest.TestCase):

    def test_empty_string_01(self):
        try:
            test_case = Strings()
            test_case.set_lines("", "A")
            test_case.comparison_code()
            self.assertTrue(False)
        except AttributeError:
            self.assertTrue(True)

    def test_empty_string_02(self):
        try:
            test_case = Strings()
            test_case.set_lines("A", "")
            test_case.comparison_code()
            self.assertTrue(False)
        except AttributeError:
            self.assertTrue(True)

    def test_empty_string_03(self):
        try:
            test_case = Strings()
            test_case.set_lines("", "")
            test_case.comparison_code()
            self.assertTrue(False)
        except AttributeError:
            self.assertTrue(True)

    def test_equals_00(self):
        try:
            test_case = Strings()
            test_case.set_lines("1.1", "1.1")
            self.assertEqual(0, test_case.comparison_code())
        except AttributeError:
            self.assertTrue(False)

    def test_equals_01(self):
        try:
            test_case = Strings()
            test_case.set_lines("1.2", "1.1")
            self.assertEqual(1, test_case.comparison_code())
        except AttributeError:
            self.assertTrue(False)

    def test_equals_02(self):
        try:
            test_case = Strings()
            test_case.set_lines("1.1", "1.2")
            self.assertEqual(2, test_case.comparison_code())
        except AttributeError:
            self.assertTrue(False)

    def test_equals_03(self):
        try:
            test_case = Strings()
            test_case.set_lines("1.1.1", "1.1")
            self.assertEqual(1, test_case.comparison_code())
        except AttributeError:
            self.assertTrue(False)

    def test_equals_04(self):
        try:
            test_case = Strings()
            test_case.set_lines("1.1", "1.1.1")
            self.assertEqual(2, test_case.comparison_code())
        except AttributeError:
            self.assertTrue(False)


### Become code Runnable
unittest.main()
