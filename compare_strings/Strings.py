class Strings:

    def set_string_1(self, text_line):
        self.one = [] if text_line is None else list(text_line)

    def set_string_2(self, text_line):
        self.two = [] if text_line is None else list(text_line)

    def set_lines(self, text_one, text_two):
        self.set_string_1(text_one)
        self.set_string_2(text_two)

    # Comparation codes
    #    0 - equals
    #    1 - "one" is greater than "two"
    #    2 - "two" is greater than "one"
    # Assumptions
    # - In case of the text do not exist (None or length zero like ""), the compare cannot be executed. Raise an AttributeError
    # - if until the final of one the other continues, the bigger will be considered greater. e.g. "ABC" is greater than "AB"
    # - The compare will be considered by each Char value. e.g. compare('a','A') results 'A' is greater than 'a';

    def __smaller(self, numOne, numTwo):
        if numOne < numTwo:
            return numOne;
        else:
            return numTwo;

    def comparison_code(self):
        # - In case of the text do not exist (None or length zero like ""), the compare cannot be executed. Raise an AttributeError
        minor = self.__smaller(len(self.one), len(self.two))

        if minor == 0:
            raise AttributeError

        for i in range(0, minor):  # the minus 1 is to adapt the size to last array position
            if (self.one[i] == self.two[i]):
                continue  # they are equal. Next compare
            return 1 if (self.one[i] > self.two[i]) else 2

        ##if arraived here and the size is the same, they are equal
        if len(self.one) == len(self.two):
            return 0
        ## If the for is over the greater will be bigger
        #    - if until the final of one the other continues, the bigger will be considered greater. e.g. "ABC" is greater than "AB"
        return 1 if len(self.one) > len(self.two) else 2

    def print(self):
        if (self.__smaller(len(self.one), len(self.two)) == 0):
            print("The phrases cannot be compared")
            return
        code = self.comparison_code()
        if code == 0:
            print("They are equals")
        else:
            print("{} is greater".format(code))

    def __init__(self):
        self.one = []
        self.two = []
