#Goal:
The goal of this question is to write a software library that accepts 2 version string as input and
returns whether one is greater than, equal, or less than the other. As an example: “1.2” is
greater than “1.1”. Please provide all test cases you could think of.

To check if is overlapped one of the position must to be inside the other interval.

## Assumptions
- In case of the text do not exist (None or length zero like ""), the compare cannot be executed. Raise an AttributeError
- if until the final of one the other continues, the bigger will be considered greater. e.g. "ABC" is greater than "AB"
- The compare will be considered by each Char value. e.g. compare('a','A') results 'A' is greater than 'a';
- The results will be returned as codes: 0,1 or 2.

### Comparation codes
    0 - equals
    1 - "one" is greater than "two"
    2 - "two" is greater than "one"
    
#Structure:

## Prerequisites
There is no necessity to install additional Python dependencies.

###Python version
Python 3.7.2.


#To Run
The _Strings.py_ is just composed by a class, to work as library. 
The _test_comparing_string.py_ is a test suite but, to simplify the execution, the it can be executed just with the command ```$ python _test_comparing_string.py_```.


#Testcases:

The test cases cover the scenarios:

- Check empty lines - cannot compare;
- Equal values - return code 0;
- Same start  but 1 has an extra character, returns code 1;
- Same start but 2 has an extra character, returns code 2;
- 1 greater than 2 based on assumptions, returns code 1;
- 2 greater than 1 based on assumptions, returns code 2;


# Missing functionalities:
Improve the tests engine to run in a bigger project. 
It was developed to support only 2 samples, it should be improved to receive many in a list, e.g..
It runs over terminal but it is not adapted for any test engine even it have extended _unittest.TestCase_.



