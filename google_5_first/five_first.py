import json

from flask import Flask, jsonify, request
from flask_cors import CORS, cross_origin
from googlesearch import search

import requests
import bs4

app = Flask(__name__)

limit = 5


@app.route('/', methods=['POST'])
@cross_origin()
def submission_req():
    try:
        data = request.get_json()
        query = data['query'].strip()
        print('Received: ' + json.dumps(data))
        urls = []

        for url in search(query, stop=limit):
            print(url)

            response = requests.get(url)
            soup = bs4.BeautifulSoup(response.text)
            # links = soup.select('html.head.title')
            urls.append({"url": url, "title": soup.title.string})

        return json.dumps({'success': True, 'query': query, 'info': urls}), 200
    except Exception as e:
        print('General error in execution: {} '.format(str(e)))
        return json.dumps({'success': False, 'message': 'Check the submission and try again'}), 500


if __name__ == '__main__':
    app.jinja_env.auto_reload = True
    app.config['TEMPLATES_AUTO_RELOAD'] = True
    app.run(host='0.0.0.0', port=9999, debug=True)
